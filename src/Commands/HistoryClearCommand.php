<?php


namespace Jakmall\Recruitment\Calculator\Commands;

use Illuminate\Console\Command;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;

class HistoryClearCommand extends Command
{
    /**
     * @var string
     */
    protected $signature;

    /**
     * @var string
     */
    protected $description;

    protected $history;

    public function __construct(CommandHistoryManagerInterface $history)
    {
        $this->history = $history;
        $this->signature = $this->getSignature();
        $this->description = "Clear saved history";
        parent::__construct();
    }

    protected function getSignature(): string
    {
        $signature = 'history:clear';
        return $signature;
    }

    public function handle(): void
    {
        $this->deleteHistoryData();
    }

    protected function deleteHistoryData(): void {
        $this->history->clearAll();
        $this->info('History cleared!');
    }
}
