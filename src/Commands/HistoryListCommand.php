<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Illuminate\Console\Command;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;

class HistoryListCommand extends Command
{
    /**
     * @var string
     */
    protected $signature;

    /**
     * @var string
     */
    protected $description;

    protected $history;

    public function __construct(CommandHistoryManagerInterface $history)
    {
        $this->history = $history;

        $this->signature = $this->getSignature();
        $this->description = "Show Calculator History";
        parent::__construct();
    }

    protected function getCommandVerb(): string
    {
        return 'history:list';
    }

    protected function getSignature(): string
    {
        $signature = "history:list {commands?* : Filter the history by commands} {--D | --driver=database : Driver for storage connection}";
        return $signature;
    }

    public function handle(): void
    {
        $commands = collect($this->getCommands())->map(
            function ($command) {
                return ucfirst($command);
            }
        )
            ->all();
        $options = $this->getOptions();
        $this->printOutput($commands, $options);
    }

    protected function getCommands(): array
    {
        return $this->argument('commands');
    }

    protected function getOptions(): string
    {
        return $this->option('driver');
    }

    protected function printOutput($commands, $options): void
    {
        $data = [];
        if (count($commands) > 0) {
            $data = $this->history->findByCommand($commands, $options);
        }
        else {
            $data = $this->history->findAll($options);
        }

        $headers = ['no', 'command', 'description', 'result', 'output', 'time'];
        $result = [];
        $no = 1;
        foreach ($data as $dataKey => $dataValue) {
            $item = [];
            foreach ($headers as $headerValue) {
                $item[$headerValue] = $dataValue[$headerValue];
            }
            $item['no'] = $no;
            $result[] = $item;
            $no++;
        }
        if (count($result) > 0) {
            $this->table($headers, $result);
        } else {
            $this->info('History is Empty');
        }
    }
}
