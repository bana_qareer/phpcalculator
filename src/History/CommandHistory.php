<?php

namespace Jakmall\Recruitment\Calculator\History;

use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;
use Doctrine\DBAL\DriverManager;
use Doctrine\DBAL\Configuration;

class CommandHistory implements CommandHistoryManagerInterface
{
    protected $storageFile;
    private $table = 'histories';
    private $database = 'sqlite:///storage/phpcalculator.sqlite';
    private $connection;
    private $queryBuilder;

    public function __construct() {
        $this->storageFile = 'storage/histories.csv';
        $config = new Configuration();

        $params = [
            'url' => $this->database
        ];

        $this->connection = DriverManager::getConnection($params, $config);
        $this->queryBuilder = $this->connection->createQueryBuilder();
    }

    public function createTable()
    {
        $sql = "CREATE TABLE IF NOT EXISTS $this->table (
            id INTEGER PRIMARY KEY,
            command VARCHAR (255) NOT NULL,
            description VARCHAR (255) NOT NULL,
            result VARCHAR (255) NOT NULL,
            output VARCHAR (255) NOT NULL,
            time DATE NOT NULL
        )";
        $stmt = $this->connection->prepare($sql);
        $stmt->execute();
    }

    public function findAll($driver = null): array
    {
        // TODO: Implement findAll() method.
        if (!empty($driver) && $driver   == 'file') {
            return $this->findFromFile();
        }

        $results = $this->queryBuilder
            ->select('id', 'command', 'description', 'result', 'output', 'time')
            ->from($this->table)
            ->execute();
        return $results->fetchAll();
    }

    public function log($command): bool
    {
        // TODO: Implement log() method.
        try {
            $this->saveToFile($command);
            $this->saveToDb($command);
            return true;
        }
        catch (Exception $e) {
            return false;
        }
    }

    public function clearAll(): bool
    {
        // TODO: Implement clearAll() method.
        try {
            $this->queryBuilder->delete($this->table)->execute();
            if (file_exists($this->storageFile)) {
                unlink($this->storageFile);
            }
            return true;
        }
        catch (Exception $e) {
            return false;
        }
    }

    public function findByCommand($command, $option = null): array
    {
        if (!empty($option) && $option == 'file') {
            return $this->findFromFile($command);
        }

        $command = str_replace(']', ')', str_replace('[', '(', stripslashes(json_encode($command))));
        $results = $this->queryBuilder
            ->select('id', 'command', 'description', 'result', 'output', 'time')
            ->from($this->table)
            ->where('command IN '.$command)
            ->execute();
        return $results->fetchAll();
    }

    protected function findFromFile($command = null)
    {
        $loadFile = file_get_contents($this->storageFile);
        $datas = explode("\n", $loadFile);
        $result = [];
        foreach ($datas as $data) {
            $data = explode(',', $data);
            if (count($data) > 1) {
                if (empty($command)) {
                    array_push($result, [
                        'command' => $data[0],
                        'description' => $data[1],
                        'result' => $data[2],
                        'output' => $data[3],
                        'time' => $data[4]
                    ]);
                }
                else {
                    if (in_array($data[0], $command)) {
                        array_push($result, [
                            'command' => $data[0],
                            'description' => $data[1],
                            'result' => $data[2],
                            'output' => $data[3],
                            'time' => $data[4]
                        ]);
                    }
                }
            }
        }
        return $result;
    }

    protected function saveToFile($data)
    {
        $dataToWrite = implode(',', $data)."\n";
        file_put_contents($this->getStorageFile(), $dataToWrite, FILE_APPEND | LOCK_EX);
    }

    protected function saveToDB($data)
    {
        try {
            $queryBuilder = $this->queryBuilder->insert($this->table)
                ->setValue('command', '?')
                ->setValue('description', '?')
                ->setValue('result', '?')
                ->setValue('output', '?')
                ->setValue('time', '?')
                ->setParameter(0, $data['command'])
                ->setParameter(1, $data['description'])
                ->setParameter(2, $data['result'])
                ->setParameter(3, $data['output'])
                ->setParameter(4, $data['time']);
            return $queryBuilder->execute();
        }
        catch (Exception $e) {
            print_r($e->getMessage());
            return false;
        }
    }

    protected function getStorageFile()
    {
        return $this->storageFile;
    }

    public function findById($id): array
    {
        $data = $this->queryBuilder
            ->select('id', 'command', 'description', 'result', 'output', 'time')
            ->from($this->table)
            ->where('id = ?')
            ->setParameter(0, $id)
            ->execute();
        return $data->fetch();
    }

    public function deleteById($id): bool
    {
        $this->deleteByIdFromFile($id);
        $deleteRecord = $this->queryBuilder
            ->delete($this->table)
            ->where('id = ?')
            ->setParameter(0, $id)->execute();
        if (!$deleteRecord) {
            return false;
        }
        return true;
    }

    protected function deleteByIdFromFile ($id): bool
    {
        $dataFromDb = $this->queryBuilder
            ->select('command', 'description', 'result', 'output', 'time')
            ->from($this->table)
            ->where('id = ?')
            ->setParameter(0, $id)
            ->execute()->fetch();
        if(!$dataFromDb){
            return false;
        };
        $value = implode(',', $dataFromDb);

        $loadFile = file_get_contents($this->storageFile);
        $data = array_values(array_filter(explode("\n", $loadFile)));

        if (($key = array_search($value, $data)) !== false) {
            unset($data[$key]);
        }
        file_put_contents($this->getStorageFile(), '');
        foreach ($data as $history) {
            file_put_contents($this->getStorageFile(), $history."\n", FILE_APPEND | LOCK_EX);
        }
        return true;
    }

}
