<?php

namespace Jakmall\Recruitment\Calculator\Http\Controller;

use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Request;

class CalculatorController
{
    protected $action;
    protected $operator;

    public function __construct(CommandHistoryManagerInterface $history)
    {
        $this->history = $history;
    }

    public function calculate($action, Request $body)
    {
        $this->action   = $action;
        $this->operator = $this->getOperator($action);
        $numbers        = $body->input;
        $description    = $this->getDescription($numbers);
        $result         = $this->calculateAll($numbers);

        $this->history->log([
            'command'     => $action,
            'description' => $description,
            'result'      => $result,
            'output'      => "$description = $result",
            'time'        => date('Y-m-d H:m:s')
        ]);

        $data     = [
            "command"   => $action,
            "operation" => $description,
            "result"    => $result
        ];
        $response = new Response();
        $response->setContent(json_encode($data));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    protected function calculateAll(array $numbers)
    {
        $number = array_pop($numbers);

        if (count($numbers) <= 0) {
            return $number;
        }

        return $this->calculateInput($this->calculateAll($numbers), $number);
    }

    /**
     * @param int|float $number1
     * @param int|float $number2
     *
     * @return int|float
     */
    protected function calculateInput($number1, $number2)
    {
        $operator = $this->operator;
        switch ($operator) {
            case '+':
                return $number1 + $number2;
                break;
            case '-':
                return $number1 - $number2;
                break;
            case '/':
                return $number1 / $number2;
                break;
            case '*':
                return $number1 * $number2;
                break;
            case '^':
                return pow($number1, $number2);
                break;
            default:
                break;
        }
    }

    protected function getOperator($action): string
    {
        $operator = [
            "add"      => "+",
            "subtract" => "-",
            "multiply" => "*",
            "divide"   => "/",
            "pow"      => "^"
        ];
        return $operator[$action];
    }

    protected function getDescription(array $numbers): string
    {
        return implode(sprintf(' %s ', $this->operator), $numbers);
    }
}
