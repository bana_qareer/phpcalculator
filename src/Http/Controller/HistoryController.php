<?php

namespace Jakmall\Recruitment\Calculator\Http\Controller;

use Illuminate\Http\Request;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;
use Symfony\Component\HttpFoundation\Response;

class HistoryController
{
    protected $history;

    public function __construct(CommandHistoryManagerInterface $history)
    {
        $this->history = $history;
    }

    public function index()
    {
        // todo: modify codes to get history
        $data     = $this->history->findAll();
        $response = new Response();
        $response->setContent(json_encode($data));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function show($id)
    {
        $record = $this->history->findById($id);
        $response = new Response();
        $response->setContent(json_encode($record));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function remove($id)
    {
        // todo: modify codes to remove history
        $deleteRecord = $this->history->deleteById($id);
        $response = new Response();
        if ($deleteRecord) {
            $response->setStatusCode(204);
        } else {
            $response->setStatusCode(500);
        }
        return $response;
    }
}
